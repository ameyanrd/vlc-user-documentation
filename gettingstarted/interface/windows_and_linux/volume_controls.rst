.. _volume_controls:

Volume Controls
===============

This allows you to increase or reduce the volume of the media file you are playing.

.. figure::  /static/images/interface/volumecontrols_windows.png
   :align:   center